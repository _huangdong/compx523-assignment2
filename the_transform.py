#!/usr/bin/python
# -*- coding: utf-8 -*-

from skmultiflow.transform.base_transform import StreamTransform
import numpy as np


class TheTransformFilter(StreamTransform):
    """ Transform data with Simple Moving Average (SMA) and Exponential Moving Average (EMA).

    Receives a features matrix, and transform them
    feature.

    Parameters
    ----------

    """

    def __init__(self, w=10, alpha=0.3, sma_features=[], ema_features=[]):
        super().__init__()
        # window for SMA
        self.w = w
        # alpha for EMA
        self.alpha = alpha
        # features that to create SMA
        self.sma_features = sma_features
        # cached sma
        self.sma_window_cache = None
        # features that to create EMA
        self.ema_features = ema_features
        # cached ema
        self.ema_window_cache = None
        self.ema_value_cache = None
        # current sample index
        self.T = 0
        # set this to use partial_fit_transform
        self.fit_transform = True

    def transform(self, X, update=False):
        """ Does the transformation process in the samples in X.

        Parameters
        ----------
        X: numpy.ndarray of shape (n_samples, n_features)
            The sample or set of samples that should be transformed.

        update: Boolean
            Update the counter and cache only when training.

        """
        row, col = X.shape

        # SMA calculation
        # WARN: turn off stupid E731
        # calc_sma = lambda x, w: np.convolve(x, np.ones(w), 'valid') / w
        def calc_sma(x, w):
            return (np.convolve(x, np.ones(w), 'valid') / w) if x.shape[0] >= w else np.array([])

        # cached feature values
        sma_features = X[:, self.sma_features].copy() if self.sma_window_cache is None \
            else np.r_[self.sma_window_cache, X[:, self.sma_features]].copy()
        # novel features to append
        sma_vec = np.zeros((row, len(self.sma_features)))

        if self.T >= self.w:
            # we cached a full window
            assert sma_features.shape[0] > self.w
            assert self.sma_window_cache.shape[0] == 10
            for idx, f in enumerate(sma_features.T):
                ret = calc_sma(f, self.w)[:-1]
                sma_vec[:, idx] = ret.reshape(row, 1)
        else:
            # the window won't slide right now
            for idx, f in enumerate(sma_features.T):
                # hard copy x_0 AND shrink window size for T AND normal calculation
                extra = calc_sma(f, self.w).tolist()
                sma_result = [f[0]] + [calc_sma(f, T)[0] for T in range(1, min(sma_features.shape[0], self.w))]
                if len(extra) > 0:
                    sma_result += extra[:-1]
                sma_vec[:, idx] = sma_result if len(sma_result) == row else sma_result[-row:]

        # add tne new features
        X = np.c_[X, sma_vec]

        # EMA calculation
        ema_columns = len(self.ema_features)
        ema_first_row = X[0, self.ema_features].reshape(1, ema_columns)
        # cached EMA or use first row as the cached EMA
        ema_base = self.ema_value_cache if self.ema_value_cache is not None else ema_first_row
        # for the convenience of calculation, place cached row or duplicated first row to row 0
        ema_features = np.r_[self.ema_window_cache if self.ema_window_cache is not None else ema_first_row,
                             X[:, self.ema_features]]

        def calc_ema(a, T, alpha):
            if T == 1:
                return ema_base if self.T == 0 else alpha * a[T - 1] + (1 - alpha) * ema_base
            else:
                return alpha * a[T - 1] + (1 - alpha) * calc_ema(a, T - 1, alpha)

        # novel features to append
        ema_vec = np.zeros((row, ema_columns))
        for r in range(row):
            ema_vec[r, :] = calc_ema(ema_features, r + 1, self.alpha)

        # append EMA to features
        X = np.c_[X, ema_vec]

        # update T
        if update:
            self.T += row
            self.sma_window_cache = sma_features[-min(self.w, sma_features.shape[0]):]
            self.ema_window_cache = ema_features[-1].reshape(1, ema_columns)
            self.ema_value_cache = ema_vec[-1].reshape(1, ema_columns)

        return X

    def partial_fit_transform(self, X, y=None, classes=None):
        """ partial_fit_transform

        Partially fits the model and then apply the transform to the data.

        Parameters
        ----------
        X: numpy.ndarray of shape (n_samples, n_features)
            The sample or set of samples that should be transformed.

        y: Array-like
            The true labels.

        classes: Not used
            make it callable from Pipeline

        Returns
        -------
        numpy.ndarray of shape (n_samples, n_features)
            The transformed data.

        """
        X = self.transform(X, update=True)

        return X

    def partial_fit(self, X, y=None, classes=None):
        """ Partial fits the model.

        Parameters
        ----------
        X: numpy.ndarray of shape (n_samples, n_features)
            The sample or set of samples that should be transformed.

        y: numpy.ndarray (optional, default=None)
            The target values.

        classes: Needed for Pipeline

        Returns
        -------
        TheTransformer
            self

        """
        return self

    def fit(self, X, y):
        """ I guess it is just simply copied the structure of Scikit-Learn... Keep it to make pipeline work.

        Parameters
        ----------
        :param X: numpy.ndarray of shape (n_samples, n_features)
            The sample or set of samples that should be transformed.

        :param y: numpy.ndarray (optional, default=None)
            The target values.

        :return: TheTransformer
            self
        """
        return partial_fit(X, y)
