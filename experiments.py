import os
import sys
import logging
import argparse
import subprocess
import threading
import pickle
import io
import time

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from skmultiflow.core import Pipeline
from skmultiflow.trees import HoeffdingTreeClassifier
from skmultiflow.evaluation import EvaluatePrequential
from skmultiflow.data import FileStream
from skmultiflow.meta import AdaptiveRandomForestClassifier, \
    LeveragingBaggingClassifier, DynamicWeightedMajorityClassifier

from the_ensemble import TheEnsembleClassifier, DiversityMeasurement
from the_transform import TheTransformFilter

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(module)s %(levelname)-8s %(message)s')

assignment2_datasets = [
    'covtype',
    'electricity',
    'RTG_2abrupt',
    'SEA_abrupt',
    'SEA_gradual',
]


def dataset_subprocess_parallelize(datasets, n_workers, worker, arguments,
                                   log_dir=''):
    for i in range(len(datasets)):
        while threading.active_count() > n_workers:
            time.sleep(1)

        f = io.BytesIO()
        pickle.dump({**arguments, 'datasets': (datasets[i],)}, f)
        thread_tag = f'{arguments["test_name"]}-{datasets[i]}'

        def run(tag, worker_, stream):
            logging.info(f'Thread {tag} started.')
            # Because the evaluator uses matplotlib, which is not a module
            # of thread-safe implementation, we must use subprocess to isolate
            # each evaluation instance.
            log_filename = os.path.join(log_dir, f'{tag}.log')
            with open(log_filename, 'w') as log:
                with subprocess.Popen(worker_, stdout=log,
                                      stderr=subprocess.STDOUT,
                                      stdin=subprocess.PIPE,
                                      text=False) as p:
                    p.communicate(stream.getvalue())
            logging.info(f'Thread {tag} finished.')

        t = threading.Thread(target=run, args=(thread_tag, worker, f))
        t.start()


def evaluate(models, model_names, metrics, max_samples=30000, show_plot=False,
             test_name='', save_plots=False, datasets=None,
             pipe=False, n_workers=1, worker=None, pipelines=None, **kwargs):
    args_to_pickle = ['models', 'model_names', 'metrics', 'max_samples',
                      'show_plot', 'test_name', 'save_plots', 'kwargs']
    not_pickled = {'datasets', 'pipe', 'n_workers', 'worker', 'pipelines',
                   'args_to_pickle', 'not_pickled'}

    assert set(locals().keys()).difference(set(args_to_pickle)) == not_pickled, \
        'Newly added function arguments should be added to either ' \
        '"args_to_pickle" or "not_pickled".'

    out_dir = 'outputs'
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    datasets = datasets if datasets else assignment2_datasets

    if not pipe and n_workers > 1:
        # Use sub process to evaluate
        if worker is None:
            worker = ['python', __file__, '--pipe']

        arguments = {k: d for k, d in locals().items() if k in args_to_pickle}
        dataset_subprocess_parallelize(datasets, n_workers, worker, arguments,
                                       log_dir=out_dir)
        return

    if pipe:
        objs_from_pickle = pickle.load(sys.stdin.buffer)

        datasets = objs_from_pickle.pop('datasets')

        models, model_names, metrics, max_samples, \
            show_plot, test_name, save_plots, kwargs = [
                objs_from_pickle.pop(i) for i in args_to_pickle]

        if objs_from_pickle:
            raise RuntimeError(f'Unconsumed values in objs_from_pickle: {objs_from_pickle}')

    file_prefix = f'{test_name}-' if test_name else ''
    batch_size = kwargs.pop('batch_size', 1)

    try:
        from evaluate_prequential_debug import EvaluatePrequentialDebug
        evaluator_type = EvaluatePrequentialDebug
    except (ModuleNotFoundError, ImportError):
        # fallback
        evaluator_type = EvaluatePrequential

    for dataset in datasets:
        evaluator_kwargs = {}
        if save_plots and evaluator_type is not EvaluatePrequential:
            output_fig = os.path.join(out_dir, f'{file_prefix}{dataset}.png')
            evaluator_kwargs = {**evaluator_kwargs, 'output_fig': output_fig}

        stream = FileStream(f'datasets/{dataset}.csv')
        evaluator = evaluator_type(max_samples=max_samples, pretrain_size=0,
                                   n_wait=100,
                                   show_plot=show_plot,
                                   metrics=metrics,
                                   batch_size=batch_size,
                                   **evaluator_kwargs)
        logging.info(f'Evaluating {dataset}')
        for m in models:
            m.reset()

        if pipelines is not None:
            evaluator.evaluate(stream=stream,
                               model=pipelines,
                               model_names=model_names)
        else:
            evaluator.evaluate(stream=stream,
                               model=models,
                               model_names=model_names)

        for m in models:
            if isinstance(m, TheEnsembleClassifier) and m.measure_diversity:
                m.diversity.export_kappa_history(os.path.join(
                    out_dir, f'{file_prefix}{dataset}-{m.tag}-kappa.txt'))


def experiment_1(show_plot=False, max_samples=30000, save_plots=False,
                 **kwargs):
    test_name1, test_name2, test_name3 = [f'exp1-{i}' for i in (1, 2, 3)]
    evaluate_kwargs = {'show_plot': show_plot, 'max_samples': max_samples,
                       'save_plots': save_plots,
                       **kwargs
                       }

    print('Experiment 1.1 Started.')
    ht = HoeffdingTreeClassifier()
    sizes = (5, 10, 20, 30)
    ensembles = [TheEnsembleClassifier(n_estimators=size, replacement_length=1000,
                                       tag=f'TE_s{size}')
                 for size in sizes]
    models = [ht, *ensembles]
    model_names = ['HT', *[model.tag for model in ensembles]]
    evaluate(models, model_names, ['accuracy'], test_name=test_name1,
             **evaluate_kwargs)

    print('Experiment 1.2 Started.')
    seeds = list(range(1, 6))
    models = [TheEnsembleClassifier(n_estimators=20, replacement_length=1000,
                                    random_state=seed, tag=f'TE_rs{seed}')
              for seed in seeds]
    model_names = [model.tag for model in models]
    evaluate(models, model_names, ['accuracy'], test_name=test_name2,
             **evaluate_kwargs)

    print('Experiment 1.3 Started.')
    lengths = (500, 1000, 2000, 5000, 10000)
    models = [TheEnsembleClassifier(n_estimators=20, replacement_length=length,
                                    tag=f'TE_l{length}')
              for length in lengths]
    model_names = [model.tag for model in models]
    evaluate(models, model_names, ['accuracy'], test_name=test_name3,
             **evaluate_kwargs)


def experiment_2(**kwargs):
    test_name = f'exp2'
    evaluate_kwargs = {**kwargs}

    print('Experiment 2 Started.')
    arf = AdaptiveRandomForestClassifier(n_estimators=20)  # FIXME: "subspace m = 60%"?
    lb = LeveragingBaggingClassifier(n_estimators=20)
    dwm = DynamicWeightedMajorityClassifier(n_estimators=20)
    te = TheEnsembleClassifier(n_estimators=20, replacement_length=1000,
                               measure_diversity=False)

    models = [arf, lb, dwm, te]
    model_names = ['ARF', 'LB', 'DWM', 'TE']
    evaluate(models, model_names, ['accuracy', 'running_time'],
             test_name=test_name, **evaluate_kwargs)


def experiment_filter(show_plot=False, max_samples=30000, save_plots=False, sma_ema_list=None, **kwargs):
    dataset = ['electricity']

    test_name = f'exp3'
    evaluate_kwargs = {'show_plot': show_plot, 'max_samples': max_samples,
                       'save_plots': save_plots,
                       **kwargs
                       }

    print('Experiment 3 Started.')
    ht = HoeffdingTreeClassifier()
    arf = AdaptiveRandomForestClassifier(n_estimators=20)  # FIXME: "subspace m = 60%"?
    lb = LeveragingBaggingClassifier(n_estimators=20)
    dwm = DynamicWeightedMajorityClassifier(n_estimators=20)
    te = TheEnsembleClassifier(n_estimators=20, replacement_length=1000,
                               measure_diversity=False)

    models = [ht, arf, lb, dwm, te]
    model_names = ['HT', 'ARF', 'LB', 'DWM', 'TE']
    transformers = []
    transform_names = []
    if sma_ema_list:
        for sma, ema in sma_ema_list:
            transformers.append(TheTransformFilter(sma_features=sma, ema_features=ema))
            if len(sma) + len(ema) > 0:
                name = 'sma' + ''.join(map(str, sma)) if len(sma) > 0 else ''
                name += 'ema' + ''.join(map(str, ema)) if len(ema) > 0 else ''
                transform_names.append(name)
            else:
                assert len(sma) + len(ema) == 0
                transform_names.append('noTrans')

    for t, n in zip(transformers, transform_names):
        pipelines = [Pipeline([('Transformer', t), ('Classifier', classifier)]) for classifier in models]
        pipeline_names = [model + '-' + n for model in model_names]
        evaluate(models, pipeline_names, ['accuracy', 'running_time'], pipelines=pipelines, datasets=dataset,
                 test_name=test_name, **evaluate_kwargs)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--show_plot', dest='show_plot', type=bool, default=False,
                        help='Show plot')
    parser.add_argument('-m', '--max_samples', type=int, default=3000,
                        help='Maximum number of samples to feed into the evaluator')
    parser.add_argument('-s', '--save_plots', dest='save_plots', action='store_true',
                        help='Save all plots to files.')
    parser.add_argument('-b', '--batch_size', type=int, default=1,
                        help='Batch size in evaluation.')
    parser.add_argument('--dpi', type=int, default=None,
                        help='Set matplotlib dpi.')
    parser.add_argument('-p', '--pipe', dest='pipe', action='store_true',
                        help='Evaluate models serialized from piped stdin.')
    parser.add_argument('-j', '--n_workers', dest='n_workers', type=int, default=1,
                        help='Number of parallel execution subprocess')
    parser.add_argument('--worker', type=str, default=None,
                        help='Define the worker command. If None, "python __file__" will be used.')
    parser.add_argument('-t', '--test', type=int, dest='test', default=0,
                        help='Specify which test to run. 0 for all.')
    return parser.parse_args()


def main(**kwargs):
    args = parse_args()

    show_plot = args.show_plot
    max_samples = args.max_samples
    save_plots = args.save_plots
    batch_size = args.batch_size

    if os.uname()[1] in ('weka9',):
        plt.rcParams['figure.dpi'] = 192
    elif args.dpi:
        plt.rcParams['figure.dpi'] = args.dpi

    if args.pipe:
        evaluate((), None, None, pipe=True)
        return
    else:
        kwargs = {**kwargs, 'n_workers': args.n_workers, 'worker': args.worker}

    # evaluate([HoeffdingTreeClassifier(), TheEnsembleClassifier(n_estimators=1)],
    #          ['HT', 'TE'], ['accuracy'])

    tests = args.test
    if tests == 0:
        tests = [1, 2, 3]
    else:
        tests = [tests]

    if 1 in tests:
        experiment_1(show_plot=show_plot, max_samples=max_samples,
                     save_plots=save_plots, batch_size=batch_size, **kwargs)
    if 2 in tests:
        experiment_2(show_plot=show_plot, max_samples=max_samples,
                     save_plots=save_plots, batch_size=batch_size, **kwargs)

    if 3 in tests:
        experiment_filter(show_plot=show_plot, max_samples=max_samples, save_plots=save_plots)


if __name__ == '__main__':
    main()
