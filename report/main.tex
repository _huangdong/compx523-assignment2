\documentclass[10pt]{article}
\usepackage{UF_FRED_paper_style}

\usepackage{lipsum}  %% Package to create dummy text (comment or erase before start)

%% ===============================================
\graphicspath{{images/}}
\onehalfspacing
\setlength{\droptitle}{-5em} %% Don't touch
%% ===============================================

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET THE TITLE
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TITLE:
\title{2nd Assignment COMPX523-20A \\[1cm]
\normalsize
  \textbf{Group Project}
}

% AUTHORS:
\author{Dong Huang\\ (1527082)\\
    \scalebox{0.8}{\href{mailto:dh146@students.waikato.ac.nz}{ \texttt{dh146@students.waikato.ac.nz}}}
\and Shuang Wu\\ (1517972)\\
    \scalebox{0.8}{\href{mailto:sw301@students.waikato.ac.nz}{ \texttt{sw301@students.waikato.ac.nz}}}
}
% DATE:
\date{\today}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ABSTRACT
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\setstretch{.8}
\maketitle
% %%%%%%%%%%%%%%%%%%
}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BODY OF THE DOCUMENT
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --------------------
\section{Introduction}
% --------------------

% This is how we ref the Figure ~\ref{fig:em}.

% \begin{figure}[H]
%     \centering
%         \includegraphics[width=.3\textwidth]{p0}
%     \caption{Example figure.}
%     \label{fig:em}
% \end{figure}

The goal of this assignment is to implement a novel ensemble classifier, and examine the improvement in performance, as well as the ability that the classifier handles concept drift. Concept drift is a classic problem that has been researched and discussed in the field of data stream mining. Streaming Ensemble Algorithm (SEA) was firstly proposed by \citet{sea}, and then using the ensemble method of multiple classifiers to handle concept drift is popularly studied. This project implements an ensemble method using a simple weight voting, and update the classifiers dynamically by evaluating the predictive performances.
% Followed by \citet{BifetAlbert2010Nemf} methods are \citet{AbbaszadehOmid2015Aemf} -- similiar research here

As a group project, a second task of feature transformation is also implemented. This approach is to calculate some metric of statistics on features and to add the results as novel features into the dataset. Experiments are included to research the efficiency of this method.

The implementation is using Python, based on the development version v5.0.dev0 of Scikit-Multiflow~\citep{skmultiflow}.

% % --------------------
% \section{Background Study}
% % --------------------

% This is how we cite with parentheses \citep{gomes2017survey}, or cite with text \citet{margineantu1997pruning}.


% --------------------
\section{Methodology}
% --------------------

This project consists of two parts: an ensemble classifier and a transform filter. Relevant test code is implemented in a standalone Python file.

\subsection{Implementation of The Ensemble Classifier}\label{subsec:implementation-of-the-ensemble-classifier}

The ensemble classifier, implemented as the class \texttt{TheEnsembleClassifier} in this project, extends the generic type of classifier \texttt{ClassifierMixin} in \texttt{Scikit-Multiflow}, overriding the methods of \texttt{predict()} and \texttt{predict\_proba()} for prediction, and the method \texttt{partial\_fit()} for training.
The class also extends \texttt{BaseSKMObject} from which the method \texttt{reset()} is inherited in order to be able to re-initialise during runtime.

The ensemble creates and maintains a set of base learners that can be any type of classifiers extending from \texttt{ClassifierMixin}, while it would be the \texttt{HoeffdingTreeClassifier} if not designated.
Diversity is introduced by randomisation of parameters of the base learners.
For base learners of the type of Hoeffding Tree, the parameters to be randomised are: the grace period, the split confidence and the tie threshold.

The performance of each single base learner in the ensemble is monitored by an associated performance evaluator upon every time a new batch of training data arrives.
The performance metric to be monitored is the prediction accuracy by default, but can be also set to any other metric that is supported by the performance evaluator.

The base learners will make their own predictions when the ensemble is called for predicting.
The predictions from the base learners are then weighted by each base learner's performance metric measured by the corresponding performance evaluator described in above in order to vote for the final prediction of the ensemble.
The variance of prediction error from the base learners can be reduced or even minimised if the base learners are uncorrelated enough in terms of errors, and thus the output performance of the ensemble is improved compared to individual classifiers.

\subsubsection{Dynamic replacement of base learner}

In addition to the base learners in the ensemble, there is also a candidate learner created in the background.
The candidate is trained by the same data as those in the ensemble and evaluated for its performance, except that it is not involved in making the ensemble's prediction.

There is a configurable period for the ensemble classifier, in the unit of number of instances, for dynamically replacing a base learner with relatively poor performance when necessary.
At the end of every period, the performance of all the base learners are assessed, and the worst in the ensemble will be replaced by the candidate \textemdash if the candidate performs better indicated by the metric;
otherwise, the ensemble keeps the same members and another candidate is created with newly randomised parameters.

\subsubsection{Measuring diversity}
A class \texttt{DiversityMeasurement} is also implemented for attaching to an ensemble classifier to measure the diversity among the base learners in the ensemble.
This measurement class maintains a series of ``contingency table''s \textemdash one for each pair of base learners (unordered) \textemdash which will be updated with the predictions made by the base learners every time the ensemble is trained.

The Cohen's Kappa statistic $\kappa$~\citep{Cohen1960} is then computed to measure the disagreements between any particular pair of base learners, based on the contingency table.
As those in a common confusion matrix, the numbers of predictions along the diagonal of the contingency table, reflect the observed agreements between the pair of predictors on the classes.
The expected agreement by chance can be measured from the normalised multiplication of the counts of predicted classes made by each base learners, i.e., from the diagonal elements of the outer product of the numbers of predicted classes.

Mathematically, using
\[
    \Theta_1 = \frac{1}{n} \sum_{i=1}^{k} C_{i,i}
\]

representing the observed agreement, where $n$ is the number of instances, $k$ is the number of classes and $C_{i, j}$ is an element in the contingency table (specific to each pair of base learners), and
\[
    \Theta_2 = \sum_{i=1}^{k} E_{i, i}
\]

representing the expected agreement by chance, where $E_{i, j} = \frac{1}{n^2}\sum_{m=1}^{k} C_{i, m} \cdot \sum_{m=1}^{k} C_{m, j}$, the Cohen's Kappa between the pair of predictors concerned can be computed as
\[
    \kappa = \frac{\Theta_1 - \Theta_2}{1 - \Theta_2}.
\]

Indices of both $C_{i, j}$ and $E_{i, j}$ in above are one-based.

The diversity measurement class is implemented standalone so that it can be attached to any other type of ensemble classifiers but not only the \texttt{TheEnsembleClassifier}.

\subsection{Implementation of Rolling Window Features}

The rolling window features are designed to use calculate the Simple Moving Average (SMA) and the Exponential Moving Average (EMA) for the specified features. The filter is extended from \textbf{StreamTransform}, offering in-place feature modification before sending to any of the learning algorithms. Scikit-Multiflow manipulates vector arithmetic using the Numpy library \citep{numpy}, which offers a convenient way to calculate SMA with a giving window size as below:
\begin{minted}{Python}
    def calc_sma(x, w):
        return np.convolve(x, np.ones(w), 'valid') / w
\end{minted}

However, while processing data in streaming, cumulated data index $T$ should take into account with the sliding windows size $w$. The pseudo-code of SMA calculation is listed below (Algorithm \ref{sma}):

\begin{algorithm}
\caption{The pseudo code for SMA calculation}\label{sma}
\begin{algorithmic}[1]
    \State append new instance to instance cache
    \If {$T = 1$}
        \State $T = x_0$
    \ElsIf {$T \geq w$}
        \State $smc\_vec = calc\_sma(x, T, w)[:-1]$
    \Else
        \For {$i \gets 1, min(T, w)$}
            \State $x' = calc\_sma(x, i, w)[0]$    \Comment{Calculate features incrementally while $T < w$}
        \EndFor
        \State $smc\_vec = x_0 +x' + calc\_sma(x, T, w)[:-1]$
    \EndIf
\end{algorithmic}
\end{algorithm}


The calculation of EMA is more straight forward: given a user specified coefficient $0 < \alpha < 1$, calculate the incremental linear combination. the pseudo-code is listed as Algorithm \ref{ema}:

\begin{algorithm}
\caption{The pseudo code for EMA calculation}\label{ema}
\begin{algorithmic}[1]
    \State $ema\_base \gets the~ first~ instance~ or~ the~ last~ cached~ instance$
    \Procedure{$calc\_ema$}{$x, T, \alpha$}\Comment{A limited recursion.}
    \If {$T = 1$}
        \State \textbf{return} $ema\_base$
    \Else
        \State \textbf{return} $\alpha \times x_{[T-1]} + (1 - \alpha) \times calc\_ema(x, T - 1, alpha)$
    \EndIf
    \EndProcedure
\end{algorithmic}
\end{algorithm}

The transform filter will then append the calculated vectors to the end of the feature vectors.

% --------------------
\section{Experiment Results and Discussions}
% --------------------

\subsection{Experiment 1: TheEnsemble variations and sanity check}

In Experiment 1, different combinations of parameters of the \texttt{TheEnsembleClassifier} are experimented.
The parameters include:
\begin{itemize}
    \item The size, i.e., the number of base learners, in the ensemble. This parameter will be referred as $S$ throughout this section.
    \item The initial $seed$ input to the random generators.
    \item The length of window to consider a dynamic replacement. This parameter will be referred $l$ throughout this section.
\end{itemize}

Total three run sessions are carried out, corresponded to the above three parameters respectively.
These sessions share the same parameters except those to be varied.
The shared parameters and settings include:

\begin{itemize}
\item Ensemble size $S$ is 20
\item Replacement window length $l$ is 1000
\item Initial $seed$ is 0
\item The maximum number of samples to run is set to 100,000\footnote{This is also the total number of samples in datasets \textit{RTG\_2abrupt}, \textit{SEA\_gradual} and \textit{SEA\_abrupt}. The \textit{electricity} has only 45,312 samples, while the \textit{covtype} has a far more number of 581,012.}
\end{itemize}

Results from the three sessions are collected in Table \ref{table:e1t1}, Table \ref{table:e1t2} and Table \ref{table:e1t3} respectively, with the parameters under experiment listed in the corresponding columns.

\begin{table}[!htb]
    \centering
    \begin{tabular}{c | c c c c c}
        \bfseries {Dataset} & \bfseries {HT} & \bfseries {TE($S$=5)} & \bfseries {TE($S$=10)} & \bfseries {TE($S$=20)} & \bfseries {TE($S$=30)} \\ \hline\\
        covtype       & 0.8366 & 0.9265 & 0.9357 & 0.9377 & 0.9381 \\
        electricity   & 0.7791 & 0.8453 & 0.8434 & 0.8348 & 0.8309 \\
        RTG\_2abrupt  & 0.5372 & 0.7775 & 0.7582 & 0.7356 & 0.7489 \\
        SEA\_abrupt   & 0.8587 & 0.8761 & 0.8772 & 0.8761 & 0.8765 \\
        SEA\_gradual  & 0.8591 & 0.8753 & 0.8759 & 0.8744 & 0.8741 \\ 
    \end{tabular}
    \caption{Experiment 1-1: Performance of TheEnsembleClassifier in different sizes.}
    \label{table:e1t1}
\end{table}

\begin{table}[!htb]
    \centering
    \begin{tabular}{c | c c c c c}
        \bfseries {Dataset} & \bfseries {TE($seed$=1)} & \bfseries {TE($seed$=2)} & \bfseries {TE($seed$=3)} & \bfseries {TE($seed$=4)} & \bfseries {TE($seed$=5)} \\ \hline\\
        covtype &  0.938($\pm$0.075) &  0.934($\pm$0.082) &  0.937($\pm$0.076) &  0.937($\pm$0.077) &  0.937($\pm$0.075) \\
 electricity &  0.834($\pm$0.098) &  0.821($\pm$0.097) &  0.836($\pm$0.096) &  0.833($\pm$0.096) &  0.835($\pm$0.097) \\
 RTG\_2abrupt &  0.766($\pm$0.185) &  0.712($\pm$0.211) &  0.771($\pm$0.188) &  0.726($\pm$0.206) &  0.767($\pm$0.185) \\
  SEA\_abrupt &  0.877($\pm$0.051) &  0.873($\pm$0.048) &  0.879($\pm$0.048) &  0.877($\pm$0.049) &  0.877($\pm$0.050) \\
 SEA\_gradual &  0.876($\pm$0.048) &  0.872($\pm$0.046) &  0.876($\pm$0.047) &  0.875($\pm$0.047) &  0.877($\pm$0.047) \\
    \end{tabular}
    \caption{Experiment 1-2: Performance of TheEnsembleClassifier initialised with different seeds.}
    \label{table:e1t2}
\end{table}

\begin{table}[!htb]
    \centering
    \begin{tabular}{c | c c c c c}
        \bfseries {Dataset} & \bfseries {TE($l$=500)} & \bfseries {TE($l$=1000)} & \bfseries {TE($l$=2000)} & \bfseries {TE($l$=5000)} & \bfseries {TE($l$=10000)} \\ \hline\\
        covtype       & 0.9378 & 0.9340 & 0.9372 & 0.9369 & 0.9373 \\
        electricity   & 0.8469 & 0.8347 & 0.8288 & 0.8250 & 0.8236 \\
        RTG\_2abrupt  & 0.7266 & 0.7556 & 0.7597 & 0.7164 & 0.7007 \\
        SEA\_abrupt   & 0.8776 & 0.8772 & 0.8800 & 0.8780 & 0.8752 \\
        SEA\_gradual  & 0.8754 & 0.8756 & 0.8774 & 0.8753 & 0.8740 \\ 
    \end{tabular}
    \caption{Experiment 1-3: Performance of TheEnsemblerClassifier with different lengths of replacement window.}
    \label{table:e1t3}
\end{table}

\begin{figure}[!htb]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{exp1-1-covtype.png}
        \caption{covtype}
        \label{fig:exp1-1-covtype}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{exp1-1-electricity.png}
        \caption{electricity}
        \label{fig:exp1-1-electricity}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{exp1-1-RTG-2abrupt.png}
        \caption{RTG\_2abrupt}
        \label{fig:exp1-1-RTG}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{exp1-1-SEA-abrupt.png}
        \caption{SEA\_abrupt}
        \label{fig:exp1-1-SEA-abrupt}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{exp1-1-SEA-gradual.png}
        \caption{SEA\_gradual}
        \label{fig:exp1-1-SEA-gradual}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{exp1-1-legend.png}
        \caption{Legend for the figures (a) to (e)}
        \label{fig:exp1-1-legend}
    \end{subfigure}
    \caption{Accuracy of HoeffdingTree and TheEnsemble classifiers}
    \label{fig:exp-1-1}
\end{figure}



\subsubsection*{Discussions}

\begin{itemize}
    \item How does TheEnsemble compares against a single hoeffding tree? Also, does TheEnsemble improve results if more base models are available? Present a table with results varying from $S$ = \{5, 10, 20, 30\}, where $S$ is the total number of learners.

    \textbf{A:} It is very clear that TheEnsemble classifier outperforms a single classifier of Hoeffding Tree, notably on datasets \textit{covtype}, \textit{electricity} and \textit{RTG\_2abrupt}, and moderately on datasets \textit{SEA\_abrupt} and \textit{SEA\_gradual}.
%    As far as the ensemble sizes tested in this experiment ranging from 5 to 30 are concerned, there is no significant improvement neither degradation observed as the size varies.
    From Table \ref{table:e2t1} in which only the accuracies averaged over the entire test are listed, there seems no significant improvement neither degradation observed as the size varies.
    However, by investigating the plots of the accuracy over time, as shown in Figure \ref{fig:exp-1-1}, there is some small but sustained difference between the curves related to the classifiers, instead of being just interleaving with fluctuations.
    For example, on the \textit{covtype} dataset, the accuracy of the ensemble with $S=5$ is always lower than the other ensembles with a larger size.
    On \textit{RTG\_2abrupt}, the situation is different: the ensemble with $S=5$ performs better than others in most of the time.
    These time diagrams, however, do not suggest a consistent and universal bonding between the performance and the size of the ensembles.
    And such fact may imply that the performance of the ensemble, improved by reducing the variance of prediction errors of the base learners, has already been saturated or near-saturated even at the size of 5.


    \item What is the impact of the randomization strategy on the diversity of the base learners? 1st repeat each experiment 5 times varying the random seed of the classifier and present a table with the average and standard deviation of each result. 2nd Present a plot, for each dataset, depicting the average Kappa statistic over time to support your claim.

    \textbf{A:} The performance of TheEnsemble classifiers initialised with the required seeds are listed in Table \ref{table:e1t2}, with the averages and standard deviations shown, and the Kappa statistics are plotted in Figure \ref{fig:kappa} for each dataset respectively, averaging from the total $\binom{20}{2} = 190$ pairs of base learners in each ensemble.
    Generally, a well-designed ensemble classifier should deliver stable performance no matter what seed it is initialised with.
    From the figures, it seems that the ensemble initialised with the random seed of 2 always has higher Kappa values than those with other seeds.
    However, this difference among the ensembles related to the internal agreement of base learners does not directly correspond to the performance, but is just caused by that with particular seed some base learners happen to be randomised with identical or close parameters so that they always agree with each other.
    Thus in Table \ref{table:e1t2}, it does not show a significant difference in the performance for the classifiers initialised with different seeds.

    Roughly, the agreements among the base estimators should be in a moderate level to achieve optimal performance from the ensemble \textemdash very low $\kappa$s are linked to the large variance in base estimators' predictions and thus may suggest the low performance of the algorithm of base estimator itself, while a very high $\kappa$ may suggest there is no diversity in the ensemble at all.


    \item Is the ensemble able to recover from concept drifts? Present a plot depicting the accuracy over time to support your claim. There should be a plot for each dataset comparing whichever version of TheEnsemble that produced the best results on the first table against the single hoeffding tree.

    \textbf{A:} Yes, the ensemble has the ability to recover from concept drift.
    Most datasets shown in Figure \ref{fig:exp-1-1} experience concept drifts at various levels, except the \textit{electricity}, whose the concept, in terms of long-term characteristics, is relatively stable despite of its large variance, which may be also related to short-term drift if we inspect the data in a narrower window of time.
    We can observe from the figure that the accuracy corresponding to TheEnsemble classifiers recovered from concept drifts, gradually but much faster than the single Hoeffding Tree, especially in the case of \textit{RTG\_2abrupt} (shown in Figure \ref{fig:exp1-1-RTG}) that encounters two times of dramatic concept drifts, at the timestamps of around 30,000 and 60,000 (in the unit of number of samples).

    \item What is the impact of the $l$ hyperparameter? Discuss the results varying the $l$ hyperparameter.

    \textbf{A:} Intuitively, shorter $l$ may help in faster recovery from concept drifts and thus better performance will be achieved. However, it does not show a strong influence of the hyperparameter $l$ on the overall accuracy in most cases if directly observing Table \ref{table:e1t3}, even though most datasets have concept drifts more or less as described in above.
    Such observation may also reflect the ability of TheEnsemble to filter the impulse of concept drift, because even that some members in the ensemble with lower performance in the new concept may not get replaced in time when the length of replacement window is too long, in many situations, though, their individual low performance may be hidden (partially if sometimes not fully) by the others that fit the new concept well.

    The only exception is on \textit{RTG\_2abrupt}.
    Again, there are dramatic concept drifts in this dataset, and because of this, ensemble classifiers with longer replacement windows (larger $l$s) are harder to recover, showing that such level of concept drift is too large to resist by the internal compensation from the ensemble.

    % \item (Optional) Which combination of $l$ and $S$ presents the best results for each dataset (on average). This will require another separate table where $l$ and $S$ are combined together.
\end{itemize}



\begin{figure}[!htb]
    \centering
    \begin{subfigure}{.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{kappa-covtype.png}
        \caption{covtype}
        \label{fig:kappa-covtype}
    \end{subfigure}
    \begin{subfigure}{.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{kappa-electricity.png}
        \caption{electricity}
        \label{fig:kappa-electricity}
    \end{subfigure}
    \begin{subfigure}{.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{kappa-RTG-2abrupt.png}
        \caption{RTG\_2abrupt}
        \label{fig:kappa-RTG-2abrupt}
    \end{subfigure}
    \begin{subfigure}{.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{kappa-SEA-abrupt.png}
        \caption{SEA\_abrupt}
        \label{fig:kappa-SEA-abrupt}
    \end{subfigure}
    \begin{subfigure}{.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{kappa-SEA-gradual.png}
        \caption{SEA\_gradual}
        \label{fig:kappa-SEA-gradual}
    \end{subfigure}
    \caption{Kappa statistic among base learners (average)}
    \label{fig:kappa}
\end{figure}


\subsection{Experiment 2: TheEnsemble vs. others}

This experiment selects several other algorithms including Adaptive Random Forest (ARF), Leveraging Bagging (LB) and the Dynamic Weighted Majority (DWM) to benchmark against the best ensemble configuration in the previous experiment.
To choose the best configuration from the previous experiment, we considered the following measures:

\begin{itemize}
    \item \textbf{Optimal performance.} The best configuration should have the top performances for all the datasets.
    \item \textbf{Balanced.} The best configuration should have no obvious performance degradation in different datasets, and it should have the best adaptability, even it is not the optimal result of a particular dataset, the difference from the optimal should be not big.
\end{itemize}

According to this, the configuration of ($S=20$ and $l=2000$) is chosen as the best, which has the best results in SEA\_abrupt and SEA\_gradual datasets among all configurations, yet the other results are fairly close to the optimal results.
The accuracy results are listed in Table \ref{table:e2t1} .

\begin{table}[!htb]
    \centering
    \begin{tabular}{c | c c c c}
        \bfseries {Dataset} & \bfseries {ARF} & \bfseries {LB} & \bfseries {DWM} & \bfseries {TE($S=20$ and $l=2000$)} \\ \hline\\
        covtype       & 0.9155 & 0.8176 & 0.7871 & \cellcolor{yellow} 0.9372 \\
        electricity   & 0.8214 & 0.7104 & 0.7835 & \cellcolor{yellow} 0.8288 \\
        RTG\_2abrupt  & 0.6968 & 0.5416 & 0.6643 & \cellcolor{yellow} 0.7597 \\
        SEA\_abrupt   & \cellcolor{yellow} 0.8912 & 0.7808 & 0.8811 & 0.8800 \\
        SEA\_gradual  & \cellcolor{yellow} 0.8888 & 0.7773 & 0.8770 & 0.8774 \\ 
    \end{tabular}
    \caption{Experiment 2, Performance of Adaptive Random Forest, Leveraging Bagging, Dynamic Weighted Majority and TheEnsemble.}
    \label{table:e2t1}
\end{table}

And the time results are listed in Table \ref{table:e2t2} respectively.

\begin{table}[!htb]
    \centering
    \begin{tabular}{c | c c c c}
        \bfseries {Dataset} & \bfseries {ARF} & \bfseries {LB} & \bfseries {DWM} & \bfseries {TE($S=20$ and $l=2000$)} \\ \hline\\
        covtype       & \cellcolor{cyan} 4342.69 & 10306.52 & 4496.44 &  6639.02\\
        electricity   & 1349.42 & 1580.55  & \cellcolor{cyan} 175.10  &  362.95\\
        RTG\_2abrupt  & 6542.73 & 6787.15  & \cellcolor{cyan} 2284.86 &  3089.99\\
        SEA\_abrupt   & 3501.00 & 2962.51  & \cellcolor{cyan} 411.31  &  715.53\\
        SEA\_gradual  & 3013.35 & 2732.68  & \cellcolor{cyan} 367.14  &  613.93\\
    \end{tabular}
    \caption{Experiment 2, The processing time for each ensemble and dataset.}
    \label{table:e2t2}
\end{table}

In the above tables, a table cell painted yellow indicates the best accuracy across all classifiers, while a cyan table cell means the classifier took the shortest processing time.

\subsubsection*{Discussions}

\begin{itemize}
    \item How does TheEnsemble performs in terms of predictive performance against the others?

    \textbf{A:} With a proper configuration, the predictive performance of the ensemble classifier can be better or at least equivalent to the other classifiers. 
    Table \ref{table:e2t1} shows that for \textit{covertype}, \textit{electricity}, and \textit{RTG\_2abrupt} the ensemble classifier produced the best accuracy; for \textit{SEA\_abrupt} and \textit{SEA\_gradual} datasets, the results were not the best, but only about $1\%$ lower than the optimal.

    \item In terms of time, is TheEnsemble more efficient than the other methods?

    \textbf{A:} The ensemble classifier is not the most efficient one in terms of time.
    The Dynamic Weighted Majority (DWM) is the fastest algorithm according to Table \ref{table:e2t2}, while ensemble classifier is generally the second fastest algorithm among the four methods.
    However, as shown in Table \ref{table:e2t1}, the DWM does not deliver the performance as good as the others.
    If we compare TheEnsemble against those ensemble classifiers at the similar level of performance, namely ARF and LB, the time consumption of TheEnsemble is significantly less.

    % \item (Optional) Create another table varying the number of learners in the other datasets as well. Then discuss which ensemble method scales better in terms of accuracy, i.e. produces better results as we increase the number of learners for the given datasets.
    % \item (Optional) What design choice made affect the computational resources the most for TheEnsemble? For example, one design choice is how the algorithm update learners, how diversity is induced, etc. Notice that the value of l or S is not a design choice, it is an hyperparameter configuration.
\end{itemize}


\subsection{Experiment 3: Feature engineering}

This experiment only uses the $electricity$ dataset, to test the transform filter, the first run is to test SMA and EMA transformations on single features, with the same configuration of Experiment 2. In this run, the original dataset is used as the baseline, each version of the dataset marked as $s(n)$ means the $n{\text -}th$ feature has the SMA transform, while $e(n)$ means the $n{\text -}th$ feature has the EMA transformation. The test results are list in Table \ref{table:e3single}, measuring in mean accuracy and the yellow cells indicate better accuracy achieved compared to the original dataset.

\begin{table}[!htb]
    \centering
    \begin{tabular}{l | c c c c c}
        \bfseries {Dataset} & \bfseries {HT} & \bfseries {ARF} & \bfseries {LB} & \bfseries {DWM} & \bfseries {TE} \\ \hline\\
        origin & 0.7796 & 0.8609 & 0.8142 & 0.7931 & 0.8361 \\
        s(0)   & \cellcolor{yellow} 0.7802 & 0.8540 & 0.8103 & 0.7919 & 0.8329 \\
        s(1)   & \cellcolor{yellow} 0.8140 & \cellcolor{yellow} 0.8631 & \cellcolor{yellow} 0.8188 & 0.7837 & \cellcolor{yellow} 0.8400 \\
        s(2)   & \cellcolor{yellow} 0.7821 & 0.8564 & \cellcolor{yellow} 0.8163 & 0.7828 & 0.8346 \\
        s(3)   & \cellcolor{yellow} 0.8014 & \cellcolor{yellow} 0.8643 & 0.8142 & 0.7867 & 0.8346 \\
        s(4)   & \cellcolor{yellow} 0.7830 & \cellcolor{yellow} 0.8615 & \cellcolor{yellow} 0.8174 & 0.7876 & 0.8320 \\ 
        s(5)   & \cellcolor{yellow} 0.7849 & \cellcolor{yellow} 0.8674 & \cellcolor{yellow} 0.8195 & 0.7897 & 0.8351 \\ \hdashline
        e(0)   & \cellcolor{yellow} 0.7815 & 0.8582 & 0.8119 & 0.7921 & 0.8358 \\
        e(1)   & \cellcolor{yellow} 0.8011 & \cellcolor{yellow} 0.8626 & \cellcolor{yellow} 0.8188 & 0.7831 & \cellcolor{yellow} 0.8388 \\ 
        e(2)   & \cellcolor{yellow} 0.7805 & 0.8582 & \cellcolor{yellow} 0.8167 & 0.7820 & 0.8309 \\
        e(3)   & \cellcolor{yellow} 0.7959 & \cellcolor{yellow} 0.8625 & 0.8142 & 0.7891 & \cellcolor{yellow} 0.8380 \\ 
        e(4)   & \cellcolor{yellow} 0.7801 & \cellcolor{yellow} 0.8616 & \cellcolor{yellow} 0.8169 & 0.7891 & 0.8360 \\
        e(5)   & \cellcolor{yellow} 0.7820 & \cellcolor{yellow} 0.8620 & \cellcolor{yellow} 0.8177 & 0.7905 & \cellcolor{yellow} 0.8374 \\ 
    \end{tabular}
    \caption{Experiment 3, Single feature transform run results.}
    \label{table:e3single}
\end{table}

The table shows that except for the Dynamic Weighted Majority (DWM) classifier, feature engineering can improve the average accuracy in general. For the single Hoeffding Tree classifier, transform any of the features will produce an improved result. Especially, feature $x_1$ (the \textit{nswprice} feature) using both transformation methods and $x_5$ (the \textit{transfer} feature) using EMA method have improved accuracy in all classifiers except DWM. Thus, a further step of combined use of SMA and EMA, and applying SMA and EMA at the same time is also experimented. There are 3 combinations:
\begin{itemize}
    \item Apply both SMA and EMA to one feature ($x_1$).
    \item Apply a single transform method (SMA or EMA) to multiple features ($x_1$ and $x_5$) at the same time.
    \item A fully combined configuration: apply SMA to $x_1$, and apply EMA to both $x_1$ and $x_5$.
\end{itemize}

The test results of the above combined configurations are list in Table \ref{table:e3combo}.

\begin{table}[!htb]
    \centering
    \begin{tabular}{l | c c c c c}
        \bfseries {Dataset} & \bfseries {HT} & \bfseries {ARF} & \bfseries {LB} & \bfseries {DWM} & \bfseries {TE} \\ \hline\\
        origin & 0.7796 & 0.8609 & 0.8142 & 0.7931 & 0.8361 \\
        s(1, 5)   & \cellcolor{yellow} 0.8148 & \cellcolor{yellow} 0.8649 & \cellcolor{yellow} 0.8239 & 0.7802 & \cellcolor{yellow} 0.8476 \\
        s(1) + e(1)   & 0.7791 & \cellcolor{yellow} 0.8653 & \cellcolor{yellow} 0.8202 & 0.7779 & \cellcolor{yellow} 0.8414 \\ 
        e(1, 5)   & \cellcolor{yellow} 0.7839 & \cellcolor{yellow} 0.8678 & \cellcolor{yellow} 0.8221 & 0.7800 & 0.8356 \\
        s(1) + e(1, 5)   & \cellcolor{yellow} 0.7809 & \cellcolor{yellow} 0.8628 & \cellcolor{yellow} 0.8239 & 0.7745 & \cellcolor{yellow} 0.8418 \\
    \end{tabular}
    \caption{Experiment 3, Combined configuration run results.}
    \label{table:e3combo}
\end{table}

The second run of combined use of SMA and EMA on multiple features seems to also promising, but still doesn't work for the DWM classifier.

\subsubsection*{Discussions}

\begin{itemize}
    \item Is it possible to improve the results without changing the ensemble configurations? In other words, just by transforming the data?

    \textbf{A:} Yes, it is possible. From the two runs of experiments, we can conclude that keeping the same configuration as the previous experiment but applying the SMA method to $x_1$ or applying the EMA method to $x_1$ and/or $x_5$, will improve the mean accuracy of the ensemble classifier. Furthermore, the combined use of transform methods applying to one or multiple features is also effective to improve the results. 

    \item Which transformations were the most effective? In other words, are there any specific feature that when transformed lead to better results?

    \textbf{A:} For the single feature run, the EMA transformation is more effective in general, which improves the accuracy of the tested methods (except DWM) when applied to features $x_1$, $x_3$, and $x_5$.
    Feature $x_1$ led to better performances for the ensemble method whenever SMA or EMA was applied to. Particularly, the configuration $s(1)$ got the highest accuracy improvement in the one-feature transformation run.
    It turns out that, applying SMA or EMA transform to several features can produce better performances for nearly all the classifiers except Dynamic Weighted Majority (DWM).
    The combined configuration $s(1, 5)$ made the most boosted improvement for Leveraging Bagging (LB), the Leveraging Bagging (LB), and the ensemble method.
    While the combination of SMA and EMA on features is also effective. The configuration $s(1) + e(1, 5)$ made the second best results in the second run (see Table \ref{table:e3combo}).

\end{itemize}

% % --------------------
% \section{Discussion}
% % --------------------

\subsection{Discussions of Improving Scikit-multiflow}

A few problems were found during the implementation and tests of the algorithms. Here is a list of problems that we met in development, as well as the solution and suggestions.

\begin{itemize}
    \item \textbf{Catch detailed exceptions.}
    
    Debugging a new classifier or transform filter can be exhausting while it is very hard to locate the correct exceptions. There is a \mintinline{Python}{try...except} block nested in the \mintinline{python}{EvaluatePrequential} class, but it stops to raise the detailed exception information. Thus, a new \mintinline{python}{EvaluatePrequentialDebug} is implemented, extended from the \mintinline{python}{EvaluatePrequential} class, aiming to locate runtime errors at the right position.

    \item \textbf{Work with subprocesses.}
    
    Scikit-Multiflow does not have a build-in threading mechanism, which makes it only use one core of CPU to run all the tasks. Modern computers generally have multi-core CPUs, so we carried out a \mintinline{python}{dataset_subprocess_parallelize} method to distribute experiment tasks to different cores of CPU using multiprocessing, which shortens the experiment time greatly.

    \item \textbf{The \textit{Pipeline} class.}
    
    While binding the transform filter to different classifiers, the \mintinline{python}{Pipeline} class is introduced. However, when running the experiments, we observed that the \mintinline{python}{transform} method was directly invoked twice times as expected, while the \mintinline{python}{patial_fit_transform} method was never called. It turns out to be a miss coding in the \textbf{pipeline.py} at line 168:

    \begin{minted}{python}
    if hasattr(transform, 'fit_transform'):
        Xt = transform.partial_fit_transform(Xt, y, classes=classes)
    \end{minted}

    Where \mintinline{python}{'fit_transform'} should be \mintinline{python}{'partial_fit_transform'}. To temporarily fix this problem, a spare attribute \mintinline{python}{fit_transform} was added to the transform filter to ensure the correct calling to \mintinline{python}{partial_fit_transform} function.
    
\end{itemize}


% \subsection{Future works}

% \lipsum[16] % Dummy text. Erase before write

% --------------------
\section{Conclusions}
% --------------------

In this project, an ensemble classifier with dynamic replacement and a transform filter are implemented, and experimented on the five given datasets.
Using the Hoeffding Tree as the base learner, the ensemble classifier shows significant improvement than a single learner, as well as the ability to recover from a certain amount of concept drift.
Comparison is also performed among TheEnsembleClassifier and some other prevalent implementations of ensemble classifier, and shows the implemented ensemble classifier has comparable performance and computational expense as others while sometimes even better.
Feature engineering is also experimented by adding new features to the original data by calculating SMA or EMA from the specified features using the implemented transform filter. 
Through the experiments, it is proved that feature engineering can improve the accuracy of the ensemble classifier as well as other classifiers.



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFERENCES SECTION
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\medskip

\bibliography{main}
%
%\clearpage
%\pagenumbering{roman}
%\appendix{\huge \bfseries Appendix A}


\end{document}