import sys
import warnings
from typing import Union
from copy import deepcopy
import logging

import numpy as np
from sklearn.preprocessing import normalize
from scipy.sparse import coo_matrix
import pandas as pd

from skmultiflow.trees import HoeffdingTreeClassifier
from skmultiflow.core.base import BaseSKMObject, ClassifierMixin, MetaEstimatorMixin
from skmultiflow.metrics import ClassificationPerformanceEvaluator
from skmultiflow.utils import check_random_state, get_dimensions

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(module)s %(levelname)-8s %(message)s')

params_range_ht: dict = {
    'grace_period': (10, 200, 20),
    'split_confidence': (1e-6, 1., 0.05),
    'tie_threshold': (0., 1., 0.05),
}

named_types = {'ht': HoeffdingTreeClassifier}


def _predict_proba_estimators(X, estimators, **kwargs):
    r, c = get_dimensions(X)
    if not isinstance(X, np.ndarray) or X.ndim != 2:
        X = np.array(X).reshape(r, c)

    # Shape of output: (#estimators, #samples, #classes)
    # return np.stack([e.predict_proba(X) for e in estimators])
    results = []
    n_classes = 0
    for e in estimators:
        result = e.predict_proba(X, **kwargs)
        results.append(result)
        n_classes = max(n_classes, result.shape[1])

    # Note that for some estimators the number of the predicted classes may
    # be less than the desired, due to the insufficient samples they got
    # trained by, especially for those newly spawned. Just padding zeros on
    # their output probability of the missing class.
    for i, result in enumerate(results):
        if result.shape[1] != n_classes:
            pad_width = ((0, 0), (0, n_classes - result.shape[1]))
            results[i] = np.pad(result, pad_width=pad_width, constant_values=0)

    return np.stack(results)


class DiversityMeasurement:
    def __init__(self, n_estimators):
        self.n_estimators = n_estimators
        self.contingency_tables = dict()
        self.kappa_history = dict()
        self.kappa_history_save_period = 100
        self.n_samples_seen = 0

    def update_contingency_table(self, batch_pred=None):
        """
        Maintain a number of contingency tables. There is a table between
        every two estimators. For a ensemble with K members and C candidates,
        the number of tables to keep is ((K + C)(K + C - 1) / 2), while each
        table is by itself a NxN table, where N is the number of classes.

        **IMPORTANT ASSUMPTION**:
        `n_estimators` is assumed to never change during the entire life-time
         of the ensemble once initialized.

        :param batch_pred: predictions from all estimators in the batch.
        :return: None
        """
        if batch_pred is None:
            return

        if batch_pred.shape[0] != self.n_estimators:
            raise RuntimeError('Number of saved batch predictions does not match'
                               ' the number of total estimators.')

        n_classes = np.max(batch_pred) + 1
        n_samples = batch_pred.shape[1]
        self.n_samples_seen += n_samples
        for i in range(batch_pred.shape[0]):
            for j in range(i + 1, batch_pred.shape[0]):
                cm = coo_matrix(([1] * n_samples,
                                (batch_pred[i].ravel(), batch_pred[j].ravel())),
                                shape=(n_classes, n_classes)).toarray()
                key = f'{i}_{j}'
                self.contingency_tables[key] = self._merge_contingency_table(key, cm)

        last_saved = self._last_kappa_timestamp()
        if (self.n_samples_seen - last_saved) >= self.kappa_history_save_period:
            self.update_kappa_history(self.n_samples_seen)

    def replace_contingency_table(self, old: int, new: int):
        for key in self.contingency_tables:
            i, j = key.split('_')
            if i != old and j != old:
                continue

            key_new = f'{new}_{j}' if i == old else f'{i}_{new}'
            self.contingency_tables[key] = self.contingency_tables[key_new]

    def kappa_between(self, i, j):
        key = f'{i}_{j}' if i < j else f'{j}_{i}'
        cm = self.contingency_tables[key]
        assert isinstance(cm, np.ndarray)

        n_classes = cm.shape[0]
        n_samples = cm.sum()

        observed = cm.trace() / n_samples
        expected_matrix = np.outer(cm.sum(axis=0), cm.sum(axis=1))
        expected = expected_matrix.trace() / n_samples ** 2

        kappa = (observed - expected) / (1 - expected)

        return kappa

    def clear_contingency_table(self, index: int = None, for_all=False):
        if index and for_all:
            warnings.warn('Index will be omitted when both index and for_all '
                          'are specified.')

        for key in self.contingency_tables:
            i, j = key.split('_')
            if not for_all and i != index and j != index:
                continue
            self.contingency_tables[key] = None

    def print_kappas(self):
        for i in range(self.n_estimators):
            kappas = []
            for j in range(i + 1, self.n_estimators):
                kappa = self.kappa_between(i, j)
                kappas.append(f'k_{i}_{j}={kappa:.2f}')
            print(','.join(kappas))

    def update_kappa_history(self, timestamp):
        kappas = self._calc_all_kappas()
        self.kappa_history[timestamp] = kappas

    def export_kappa_history(self, filename: str):
        df = pd.DataFrame(self.kappa_history).transpose().reset_index()

        columns = ['timestamp']
        for i in range(self.n_estimators):
            for j in range(i + 1, self.n_estimators):
                columns.append(f'k_{i}_{j}')
        df.columns = columns

        df.to_csv(filename)

    def _calc_all_kappas(self):
        kappas = []
        for i in range(self.n_estimators):
            for j in range(i + 1, self.n_estimators):
                kappas.append(self.kappa_between(i, j))

        return kappas

    def _last_kappa_timestamp(self):
        last_saved = 0
        if self.kappa_history:
            if sys.version_info[:2] >= (3, 7):
                last_saved = list(self.kappa_history.keys())[-1]
            else:
                last_saved = sorted(self.kappa_history.keys())[-1]

        return last_saved

    def _merge_contingency_table(self, key, cm_new):
        if not isinstance(cm_new, np.ndarray):
            cm_new = np.array(cm_new)

        cm = self.contingency_tables.get(key)
        if cm is None:
            return cm_new
        n_classes = max(cm.shape[0], cm_new.shape[0])

        cm1 = np.pad(cm, (0, n_classes - cm.shape[0]), constant_values=0)
        cm2 = np.pad(cm_new, (0, n_classes - cm_new.shape[0]), constant_values=0)

        return cm1 + cm2


class TheEnsembleClassifier(BaseSKMObject, ClassifierMixin):
    evaluator_class = ClassificationPerformanceEvaluator
    _n_candidates = 1  # number of candidates is fixed to 1 currently.

    def __init__(self,
                 base_estimator: Union[str, ClassifierMixin] = None,
                 base_estimator_type: type = None,
                 n_estimators=10,
                 random_state=None,
                 params_range: dict = None,
                 perf_metric: str = 'accuracy',
                 replacement_length: int = 1000,
                 tag=None,
                 measure_diversity=True,
                 **kwargs
                 ) -> None:
        super().__init__()

        if base_estimator is None and base_estimator_type is None:
            base_estimator_type = HoeffdingTreeClassifier
        self.base_estimator_type = self._determine_base_type(
            base_estimator, base_estimator_type)
        self.n_estimators = n_estimators

        self._random_state = check_random_state(random_state)
        self._init_random_state = random_state

        self.params_range = params_range

        self.ensemble = None
        self.candidates = None

        self.perf_metric = perf_metric
        self.measure_diversity = measure_diversity

        self.replacement_length = replacement_length
        self.tag = tag if tag else f'TeN{n_estimators}L{replacement_length}'
        self.n_samples_seen = 0

        self._init_kwargs = kwargs

        self._init_ensemble()

    def partial_fit(self, X, y, **kwargs):
        r, _ = get_dimensions(X)

        batch_pred = []
        for estimators in (self.ensemble, self.candidates):
            y_pred_members = _predict_proba_estimators(X, estimators).argmax(axis=-1)
            batch_pred.append(y_pred_members)

            for i, e in enumerate(estimators):
                for y_true, y_pred in zip(y.ravel(), y_pred_members[i].ravel()):
                    e.evaluator.add_result(y_true, y_pred)

                e.partial_fit(X, y, **kwargs)

        # Pre-train the level 2 candidates.
        for e in self._lv2_candidates:
            e.partial_fit(X, y, **kwargs)

        if self.measure_diversity:
            self.diversity.update_contingency_table(np.vstack(batch_pred))
        self._assess_base_replacement(inc=r)
        self.n_samples_seen += r

    def predict(self, X, **kwargs):
        perf_members = np.array([self.p(e) for e in self.ensemble]).reshape(
            (self.n_estimators, 1, 1))

        # Special treatment for initial state of all zeros of accuracy.
        if np.isclose(perf_members.sum(), 0):
            perf_members += 1

        y_pred_members = self._predict_proba_members(X, **kwargs).argmax(axis=-1)
        n_classes = np.max(y_pred_members) + 1

        # One-hot encoded predictions
        # (Reference: https://stackoverflow.com/a/37323404)
        y_pred_ensemble_1hot = np.eye(n_classes)[y_pred_members]

        # Weighted vote on classes
        y_pred = np.sum(y_pred_ensemble_1hot * perf_members, axis=0).argmax(axis=-1)

        return y_pred

    def predict_proba(self, X):
        perf_members = np.array([self.p(e) for e in self.ensemble]).reshape(
            (self.n_estimators, 1, 1))
        y_proba_members = self._predict_proba_members(X)

        y_proba_mean = (y_proba_members * perf_members).mean(axis=0)

        return normalize(y_proba_mean, norm='l1')

    def reset(self):
        self._random_state = check_random_state(self._init_random_state)
        self.n_samples_seen = 0
        self._init_ensemble()

        return self

    def spawn_estimator(self,
                        base_estimator: Union[str, ClassifierMixin] = None,
                        base_estimator_type: type = None,
                        params_range: dict = None,
                        use_ensemble_kwargs=True,
                        **kwargs):
        """
        Spawn a new instance of base estimator.
        Type of the new instance will be determined in the following order:
          - the same type of base estimator if `base_estimator` specified, or
          - specified by `base_estimator_type` if `base_estimator=None`
          - use the default type of the ensemble if the both above are `None`.

        `params_range` is dict type in the following format:
            `{param1: (low1, high1, step1), param2: (low2, high2, step2), ...}`

        If `step` is None, the parameter will be uniformly randomized between
        `low` and `high`.
        Each generated parameter will be cast to the same type of the corresponding `low`
        variable.

        :param use_ensemble_kwargs: include the ensemble's kwargs for __init__()
        :param base_estimator: either of a string or an object of existing estimator.
        :param base_estimator_type: type name of any descendants of ClassifierMixin
        :param params_range: dict of parameters to randomize.
        :return: A new instance of base estimator with randomized parameters
        """
        base_estimator_type = self._determine_base_type(
            base_estimator, base_estimator_type)

        if params_range is None:
            if base_estimator_type is HoeffdingTreeClassifier:
                params_range = params_range_ht
            else:
                params_range = self.params_range

        random_params = {k: self._rand_within(*v) for k, v in params_range.items()}
        if use_ensemble_kwargs:
            base_kwargs = {**self._init_kwargs, **random_params, **kwargs}
        else:
            base_kwargs = {**random_params, **kwargs}
        e = base_estimator_type(**base_kwargs)

        # Not a perfect practice to associate a dynamic attribute
        # but just don't want to introduce more hierarchy of inheritance.
        e.evaluator = self.evaluator_class()

        return e

    def p(self, estimator) -> float:
        assert isinstance(estimator.evaluator, self.evaluator_class)
        score = getattr(estimator.evaluator, f'{self.perf_metric}_score', None)

        if score:
            return score()
        else:
            raise ValueError(f'Evaluator has no score for {self.perf_metric}')

    def _init_ensemble(self):
        self.ensemble = [self.spawn_estimator() for _ in range(self.n_estimators)]
        self.candidates = [self.spawn_estimator() for _ in range(self._n_candidates)]

        # Level 2 candidates: they are "candidates of candidates" and are created
        # one window ahead before they become candidates. During the initial
        # window, they are just "partial fit", but not going through any
        # performance evaluation.
        # The purpose of level 2 candidates is to make sure all new candidates
        # are **pre-trained** just as those created at the very beginning, and
        # this makes it more fair in comparing the performance between the
        # candidates and the existing ensemble members.
        self._lv2_candidates = [self.spawn_estimator() for _ in range(self._n_candidates)]

        self.replacement_counter = 0

        if self.measure_diversity:
            self.diversity = DiversityMeasurement(self.n_estimators + self._n_candidates)
        else:
            self.diversity = None

    def _determine_base_type(self,
                             base_estimator: Union[str, ClassifierMixin] = None,
                             base_estimator_type: type = HoeffdingTreeClassifier,
                             ):
        # parameter base_estimator overrides base_estimator_type
        if base_estimator:
            if isinstance(base_estimator, str):
                if base_estimator not in named_types:
                    raise ValueError(f'Incorrect base_estimator={base_estimator} specified.')
                base_estimator_type = named_types[base_estimator]
            else:
                base_estimator_type = type(base_estimator)

        if base_estimator_type is None:
            base_estimator_type = self.base_estimator_type

        return base_estimator_type

    def _rand_within(self, low, high, stepping):
        rs = self._random_state
        if stepping:
            ret = low + stepping * rs.randint(int((high - low) / stepping))
        else:
            ret = rs.uniform(low, high)
        return type(low)(ret)

    def _predict_proba_members(self, X, **kwargs):
        # Shape of output: (#ensemble, #samples, #classes)
        return _predict_proba_estimators(X, self.ensemble, **kwargs)

    def _predict_proba_candidates(self, X, **kwargs):
        # Shape of output: (#candidates, #samples, #classes)
        return _predict_proba_estimators(X, self.candidates, **kwargs)

    def _assess_base_replacement(self, inc=1):
        self.replacement_counter += inc

        if self.replacement_counter >= self.replacement_length:
            perf_members = np.array([self.p(e) for e in self.ensemble])
            perf_candidates = np.array([self.p(e) for e in self.candidates])

            logging.info(self.tag + ': ' + ','.join(
                [f'p_e({i})={p:.2f}' for i, p in enumerate(perf_members)]))
            logging.info(self.tag + ': ' + ','.join(
                [f'p_c({i})={p:.2f}' for i, p in enumerate(perf_candidates)]))
            if self.measure_diversity:
                self.diversity.print_kappas()

            if perf_members.min(initial=None) < perf_candidates.max(initial=None):
                index_member = perf_members.argmin()
                index_candidate = perf_candidates.argmax()

                logging.info(f'Replacing ensemble[{index_member}] '
                             f'(@{perf_members[index_member]:.2f}) '
                             f'with candidate[{index_candidate}] '
                             f'(@{perf_members[index_candidate]:.2f}).')

                self.ensemble[index_member] = self.candidates[index_candidate]
                ct_index = index_candidate + self.n_estimators
                if self.measure_diversity:
                    self.diversity.replace_contingency_table(index_member, ct_index)

            self.replacement_counter = 0
            # Re-spawn all candidates.
            for i in range(self._n_candidates):
                self.candidates[i], self._lv2_candidates[i] = \
                    self._lv2_candidates[i], self.spawn_estimator()
                if self.measure_diversity:
                    self.diversity.clear_contingency_table(self.n_estimators + i)
